import { Autocomplete, AutocompleteItem } from "@nextui-org/react";

function App() {
	return (
		<div className="p-4 bg-zinc-500">
			<div>
				<Autocomplete
					placeholder="Selecione uma opção"
					className="max-w-xs"
					defaultItems={[
						{
							value: "One",
							label: "One",
							icon: "👋",
						},
						{
							value: "Two",
							label: "Two",
							icon: "👋👋",
						},
					]}
				>
					{(option) => (
						<AutocompleteItem
							key={option.value}
							value={option.value}
							selectedIcon={option.icon}
						>
							{option.label}
						</AutocompleteItem>
					)}
				</Autocomplete>
			</div>
		</div>
	);
}

export default App;
